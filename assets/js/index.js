$(document).ready(function () {
    $(this).scrollTop(0);

    // Scroll and change style to nav bar
    $(window).scroll(function() {
        if ($(document).scrollTop() > 0) {
            $('.header')
                .css('background', 'var(--color-back-alt)')
                .css('height', '100px');
        }else {
            $('.header')
                .css('background', 'transparent')
                .css('height', '180px');
        }
    });
});
